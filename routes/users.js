const express = require('express');
const router = express.Router();
const argon2 = require('argon2');
//const model = require('../models/index');
const { QueryTypes } = require('sequelize');
const jwt=require('jsonwebtoken');
const model = require('../models/index');
const passportJWT=require('../middlewares/passport-jwt')


//get profile(คนที่กำลังloginอยู่)
/*/api/v1/users/profile*/
router.get('/profile',[passportJWT.isLogin] ,async function(req, res, next) {
  const user=await model.User.findByPk(req.user.user_id);

  return res.status(200).json({
    user:{
      id:user.id,
      fullname:user.fullname,
      email:user.email,
      created_at:user.created_at
    }
    //user:req.user.user_id
  });
});


/* /api/v1/users/ */
router.get('/', async function(req, res, next) {
  //const users=await model.User.findAll();
  //const users=await model.User.findAll({
    //attribute: ['id','fullname']
    //attribute: {exclude:['password']},
    //order: [['id','desc']]
  //});

  //const { QueryTypes } = require('sequelize');
const sql='select id,fullname,email from `users` order by id desc';
const users = await model.sequelize.query(sql, { 
  type: QueryTypes.SELECT 
});

  const totalUsers=await model.User.count();
  //res.send('respond with a resource');
  return res.status(200).json({
      //message:'all user'
      total: totalUsers,
      data: users
  });
});

/* /api/v1/users/register */
router.post('/register', async function(req, res, next) {
  const {fullname,email,password}=req.body;
  const passwordHash = await argon2.hash(password);
//1 check emailซ้ำ
  const user = await model.User.findOne({where:{email: email}});
  if (user !==null){
    return res.status(400).json({message:'มีผู้ใช้งานemailนี้แล้ว'});
  }

//2เข้ารหัสผ่าน
//3.บันทึกลงตรางusers

const newUser=await model.User.create({
    fullname:fullname,
    email:email,
    password:passwordHash
});

  return res.status(201).json({
    user: {
      id: newUser.id,
      fullname: newUser.fullname
    },
    message:'ลงทะเบียนสำเร้จ'
  });
});

//*****login//

//api/v1/users/register */
router.post('/login', async function(req, res, next) {
  //const {fullname,email,password}=req.body;
  //const passwordHash = await argon2.hash(password);
  const {email,password}=req.body;
//1 check emailว่ามีอยู่ในระบบหรือไม่
  const user = await model.User.findOne({where:{email: email}});
  if (user ===null){
    return res.status(404).json({message:'ไม่พบผู้ใช้งานในระบบ'});
  }

//2นำรหัสผาสนมาเปลียบเทียบรหัสผ่านในฐานข้อมูล
const isValid = await argon2.verify(user.password,password);
if (!isValid){
  return res.status(401).json({message:'รหัสผ่านไม่ถูกต้อง'});
}

//3.สร้าง token
const token=jwt.sign({user_id:user.id},process.env.JWT_KEY,{expiresIn:'7d'});

  return res.status(200).json({

   // user: {
     // id: newUser.id,
      //fullname: newUser.fullname

    //},
    message:'เข้าระบบสำเร้จ',
    access_token:token
  });
});




module.exports = router;
